import React from 'react'
//kako se klika ADD se dodva vo nizata 
//a kako se dodava vo nizata taka se shiri item list
import { useDispatch,useSelector } from 'react-redux'
import { add } from '../Action'
import { useState } from 'react'

const TodoInput = () => {

  const [text, setText] = useState('')

  const dispatch = useDispatch()


const handleChange=(e)=> {
    setText(e.target.value)
  }

  return (
    <div className='form-container'><input className='input-text' onChange={handleChange} type="text" /> <button className='input-submit' onClick={() => dispatch(add(text))}>Add</button>
    
    </div>
  )
}

export default TodoInput
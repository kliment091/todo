import React from 'react'
import { useDispatch } from 'react-redux'
import { del } from '../Action'
const TodoItem = (props) => {
const dispatch=useDispatch()
const delItem=(id)=>{
return id
}
  return (
  <div className='item' >{props.name} <button onClick={()=>dispatch(del(delItem(props.id)))}>X</button></div>
  )
}

export default TodoItem
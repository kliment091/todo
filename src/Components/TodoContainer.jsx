import React from 'react'
import { useSelector } from 'react-redux'
import TodoInput from './TodoInput'
import TodoList from './TodoList'
//redux
//test


const TodoContainer = () => {
  return (
    <div className='container'>
    <div className='inner'>
        <TodoInput/>
        <TodoList/>
        </div>
    </div>
  )
}

export default TodoContainer